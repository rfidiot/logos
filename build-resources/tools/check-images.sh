#!/bin/bash

srindex=$1
checklocation=$2
saveto=$3

cd $checklocation
cat $srindex | grep '^# .*' | while read line ; do
    if ! [ -f ${line#??}.* ]; then
        echo The following logo was not found: ${line#??} >> $saveto
        echo "" >> $saveto
    fi
done

cd $checklocation
for file in * ; do
    if [ -f $file ]; then
        if ! grep -q ${file%.*} $srindex; then
            echo The following reference was not found: ${file%.*} >> $saveto
            echo "" >> $saveto
        fi
    fi
done

cd $checklocation/white
for file in * ; do
    if [ ! -f $checklocation/${file%.*}.* ]; then
        echo The following white logo has no black version: $file >> $saveto
        echo "" >> $saveto
    fi
done

cd $checklocation
for file in *.svg ; do
    if [ -f ${file%.*}.png ]; then
        echo The following black logo has an obsolete png version: $file >> $saveto
        echo "" >> $saveto
    fi
done

cd $checklocation/white
for file in *.svg ; do
    if [ -f ${file%.*}.png ]; then
        echo The following white logo has an obsolete png version: $file >> $saveto
        echo "" >> $saveto
    fi
done

cd $checklocation
for file in *.svg ; do
    if [ -f $checklocation/white/${file%.*}.png ]; then
        echo The following black logo is an svg, but has a white png: $file >> $saveto
        echo "" >> $saveto
    fi
done
